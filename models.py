from google.appengine.ext import ndb
import webapp2_extras.appengine.auth.models as auth_models
from webapp2_extras.security import generate_password_hash


class User(auth_models.User):
    """User model"""

    # model fields declaration
    name = ndb.StringProperty(required=True)
    email = ndb.StringProperty(required=True)

    def change_password(self, password):
        self.password = generate_password_hash(password, length=12)
