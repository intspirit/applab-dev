import webapp2
from webapp2_extras import sessions, auth
import jinja2
from os import path
from google.appengine.api import mail


views = jinja2.Environment(
  loader=jinja2.FileSystemLoader(path.dirname(__file__) + '/views'))


class MainHandler(webapp2.RequestHandler):
    """
    Class for handling common tasks
    """
    def dispatch(self):
        try:
            super(MainHandler, self).dispatch()
        finally:
            self.session_store.save_sessions(self.response)

    @webapp2.cached_property
    def session_store(self):
        return sessions.get_store(request=self.request)

    @webapp2.cached_property
    def session(self):
        return self.session_store.get_session(backend="securecookie")

    @webapp2.cached_property
    def auth(self):
        return auth.get_auth(request=self.request)

    @webapp2.cached_property
    def user(self):
        user = self.auth.get_user_by_session()
        return user

    @webapp2.cached_property
    def user_model(self):
        user_model, timestamp = self.auth.store.user_model.get_by_auth_token(
            self.user['user_id'],
            self.user['token']) if self.user else (None, None)
        return user_model


class MainPage(MainHandler):
    """MainPage"""
    template = views.get_template('index.html')

    def get(self):
        self.response.write(self.template.render({'user': self.user_model}))


class LogIn(MainHandler):
    """LogIn page"""
    template = views.get_template('login.html')

    def get(self):
        self.response.write(self.template.render())

    def post(self):
        remember_me = 'remember' in self.request.params

        try:
            self.auth.get_user_by_password("auth:" + \
                                       self.request.params['email'],
                                       self.request.params['password'],
                                       remember=remember_me)
        except auth.InvalidAuthIdError:
            self.response.write(self.template.render({'error': 'Invalid email/password'}))
        else:
            self.redirect('/')


class SignUp(MainHandler):
    """SignUp page"""
    template = views.get_template('signup.html')

    def get(self):
        self.response.write(self.template.render())

    def post(self):
        success, info = self.auth.store.user_model.create_user(
                "auth:" + self.request.params['email'],
                unique_properties=['email'],
                email=self.request.params['email'],
                name=self.request.params['name'],
                password_raw=self.request.params['password'])

        if success:
            self.auth.get_user_by_password("auth:" + \
                                           self.request.params['email'],
                                           self.request.params['password'])
            self.redirect('/')
        else:
            self.response.write(info)


class RemindPassword(MainHandler):
    """Password remind handler"""
    template = views.get_template('remind.html')

    def get(self):
        self.response.write(self.template.render())

    def post(self):
        email = self.request.params['email']
        user = self.auth.store.user_model.get_by_auth_id("auth:" + email)
        sender = ""  # TODO FROM FIELD
        if user:
            token = self.auth.store.user_model.create_auth_token(user.key.id())
            message = mail.EmailMessage(sender=sender,
                                        subject="Applab password reset",
                                        to=email,
                                        body=self.request.host + '/reset/' + token)
            message.send()
            self.response.write(self.template.render({'success': 'Please, check your mail'}))
        else:
            self.response.write(self.template.render({'error': 'Wrong email'}))


class ResetPassword(MainHandler):
    """Password reset handler"""
    template = views.get_template('reset.html')

    def get(self, token):
        self.response.write(self.template.render({'token': token}))

    def post(self, token):
        email = self.request.params['email']
        password = self.request.params['password']
        confirm = self.request.params['confirm']
        if password != confirm:
            self.response.write(self.template.render({'token': token, 'error': 'Password mismatch'}))
        auth_id = self.auth.store.user_model.get_by_auth_id("auth:" + email)
        user, timestamp = self.auth.store.user_model.get_by_auth_token(auth_id.key.id(), token)
        if user:
            user.change_password(password)
            user.put()
            self.auth.store.user_model.delete_auth_token(user.key.id(), token)
            self.redirect('/')


class LogOut(MainHandler):
    """ LogOut handler """
    def get(self):
        self.auth.unset_session()
        self.redirect('/')


def handle_404(request, response, exception):
    """
    Function for handling 404 server error
    """
    template = views.get_template('error.html')
    response.write(template.render({'error_code': 404, 'error_message': 'Sorry, could not find that'}))
    response.set_status(404)


def handle_500(request, response, exception):
    """
    Function for handling 500 server error
    """
    template = views.get_template('error.html')
    response.write(template.render({'error_code': 500, 'error_message': 'Please, try later'}))
    response.set_status(500)
