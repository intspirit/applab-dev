import webapp2
from controller import MainPage, LogIn, SignUp, LogOut, RemindPassword, ResetPassword, handle_404, handle_500
from models import User

config = {
    'webapp2_extras.sessions': {
        'secret_key': 'cyxVFJ8CWedYA2mISnNq65QRZtEp7GDfsihgBPTH0bL4ukjM1o'
    },
    'webapp2_extras.auth': {
        'user_model': User,
        'token_max_age': 86400 * 31 * 3  # seconds_in_a_day * days * months
    }

}

routes = [
        ('/', MainPage),
        ('/login', LogIn),
        ('/signup', SignUp),
        ('/logout', LogOut),
        ('/remind', RemindPassword),
        ('/reset/(.*)', ResetPassword)
        ]

app = webapp2.WSGIApplication(routes=routes, config=config)

app.error_handlers[404] = handle_404
app.error_handlers[500] = handle_500
